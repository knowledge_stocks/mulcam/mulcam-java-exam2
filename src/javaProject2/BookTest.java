package javaProject2;

public class BookTest {

	public static void main(String[] args) {
		Book[] books = new Book[3];
		books[0] = new Book("B001", "자바 프로그래밍", "홍길동", 2021, 25000, "멀티출판사");
		books[1] = new Book("B002", "자바스크립트", "이몽룡", 2020, 30000, "서울출판사");
		books[2] = new Book("B003", "HTML/CSS", "성춘향", 2021, 18000, "강남출판사");
		
		System.out.println("도서번호\t도서명       \t저자\t가격\t발행일\t출판사");
		System.out.println("--------------------------------------------------------");
		for (Book book : books) {
			System.out.println(book.toString());
		}

		System.out.println();
		
		Magazine[] magazines = new Magazine[3];
		magazines[0] = new Magazine("M001", "자바 월드", "홍길동", 2021, 5, 25000, "멀티출판사");
		magazines[1] = new Magazine("M002", "웹 월드", "이몽룡", 2020, 7, 30000, "서울출판사");
		magazines[2] = new Magazine("M003", "게임 월드", "성춘향", 2021, 9, 18000, "강남출판사");
		
		System.out.println("잡지번호\t잡지명\t발행인\t가격\t발행연도\t출판사\t발행월");
		System.out.println("--------------------------------------------------------");
		for (Magazine magazine : magazines) {
			System.out.println(magazine.toString());
		}
	}

}
